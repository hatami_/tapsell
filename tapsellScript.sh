#!/bin/bash
echo "1- Start confluent..."
echo ""

sudo /opt/confluent-3.3.0/bin/confluent start

echo ""
echo "---------- connected-----------------"

sleep 5

echo "2- Create tapsell topic..."
echo ""
sudo /opt/confluent-3.3.0/bin/kafka-topics --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic tapsell
