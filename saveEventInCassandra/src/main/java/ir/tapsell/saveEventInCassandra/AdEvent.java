package ir.tapsell.saveEventInCassandra;

import java.io.Serializable;
import java.text.MessageFormat;

public class AdEvent implements Serializable{
	
	private String requestId; // a unique id for the request
	private String adId;
	private String adTitle;
	private Double advertiserCost;
	private String appId;
	private String appTitle;
	private Long impressionTime;
	private Long clickedTime;

	public AdEvent() {
	}

	public AdEvent(String requestId, String adId, String adTitle, Double advertiserCost, String appId,
			String appTitle, Long impressionTime, Long clickedTime) {
		this.requestId = requestId;
		this.adId = adId;
		this.adTitle = adTitle;
		this.advertiserCost = advertiserCost;
		this.appId = appId;
		this.appTitle = appTitle;
		this.impressionTime = impressionTime;
		this.clickedTime = clickedTime;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getAdId() {
		return adId;
	}

	public void setAdId(String adId) {
		this.adId = adId;
	}

	public String getAdTitle() {
		return adTitle;
	}

	public void setAdTitle(String adTitle) {
		this.adTitle = adTitle;
	}

	public Double getAdvertiserCost() {
		return advertiserCost;
	}

	public void setAdvertiserCost(Double advertiserCost) {
		this.advertiserCost = advertiserCost;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getAppTitle() {
		return appTitle;
	}

	public void setAppTitle(String appTitle) {
		this.appTitle = appTitle;
	}

	public Long getImpressionTime() {
		return impressionTime;
	}

	public void setImpressionTime(Long impressionTime) {
		this.impressionTime = impressionTime;
	}



	public Long getClickedTime() {
		return clickedTime;
	}

	public void setClickedTime(Long clickedTime) {
		this.clickedTime = clickedTime;
	}

	@Override
	public String toString() {
		return MessageFormat.format(
				"AdEvent'{'requestId=''{0}'', adId=''{1}'', adTitle=''{2}'', advertiserCost={3}, "
				+ "appId=''{4}'', appTitle=''{5}'', impressionTime={6}, clickedTime={7}'}'",
				requestId, adId, adTitle, advertiserCost, appId, appTitle, impressionTime, clickedTime);
	}

}
