package ir.tapsell.saveEventInCassandra;

import java.io.Serializable;
import java.text.MessageFormat;

public class ImpressionEvent implements Serializable{
	
	private String requestid; // a unique id for the request
	private String adid;
	private String adtitle;
	private Double advertisercost;
	private String appid;
	private String apptitle;
	private Long impressiontime;

	public ImpressionEvent() {
	}

	public ImpressionEvent(String requestid, String adid, String adtitle, Double advertisercost, String appid,
			String apptitle, Long impressiontime) {
		this.requestid = requestid;
		this.adid = adid;
		this.adtitle = adtitle;
		this.advertisercost = advertisercost;
		this.appid = appid;
		this.apptitle = apptitle;
		this.impressiontime = impressiontime;

	}



	public String getRequestid() {
		return requestid;
	}

	public void setRequestid(String requestid) {
		this.requestid = requestid;
	}

	public String getAdid() {
		return adid;
	}

	public void setAdid(String adid) {
		this.adid = adid;
	}

	public String getAdtitle() {
		return adtitle;
	}

	public void setAdtitle(String adtitle) {
		this.adtitle = adtitle;
	}

	public Double getAdvertisercost() {
		return advertisercost;
	}

	public void setAdvertisercost(Double advertisercost) {
		this.advertisercost = advertisercost;
	}

	public String getAppid() {
		return appid;
	}

	public void setAppid(String appid) {
		this.appid = appid;
	}

	public String getApptitle() {
		return apptitle;
	}

	public void setApptitle(String apptitle) {
		this.apptitle = apptitle;
	}

	public Long getImpressiontime() {
		return impressiontime;
	}

	public void setImpressiontime(Long impressiontime) {
		this.impressiontime = impressiontime;
	}

	@Override
	public String toString() {
		return MessageFormat.format(
				"ImpressionEvent'{'requestid=''{0}'', adid=''{1}'', adtitle=''{2}'', advertisercost={3}, "
				+ "appid=''{4}'', apptitle=''{5}'', impressiontime={6}'}'",
				requestid, adid, adtitle, advertisercost, appid, apptitle, impressiontime);
	}

}
