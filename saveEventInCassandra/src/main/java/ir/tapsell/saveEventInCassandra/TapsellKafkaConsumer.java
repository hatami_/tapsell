package ir.tapsell.saveEventInCassandra;

import static com.datastax.spark.connector.japi.CassandraJavaUtil.javaFunctions;

import java.io.Serializable;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.LongDeserializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.common.xcontent.XContentType;
//import org.elasticsearch.node.NodeBuilder;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.json.JSONObject;

import com.datastax.driver.core.Session;
import com.datastax.spark.connector.cql.CassandraConnector;
import com.datastax.spark.connector.japi.CassandraJavaUtil;
import com.fasterxml.jackson.databind.ObjectMapper;

public class TapsellKafkaConsumer implements Serializable {

	private final static String TOPIC = "tapsell";
	private final static String BOOTSTRAP_SERVERS = "localhost:9092,localhost:9093,localhost:9094";
	private static String[] event = {};
	private static String strTemp = "";
	private static String keyValue = "";
	private static String reqId‬‬ = "";
	private static Long clickTime‬‬;
	private static String indexName = "event";

	private static Consumer<Long, String> createConsumer() {
		
		final Properties props = new Properties();
		props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);
		props.put(ConsumerConfig.GROUP_ID_CONFIG, "group");
		props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, LongDeserializer.class.getName());

		props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());

	
		// props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest");
		props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

		/*
		 * -earliest: automatically reset the offset to the earliest offset -latest:
		 * automatically reset the offset to the latest offset -none: throw exception to
		 * the consumer if no previous offset is found or the consumer's group
		 * 
		 */

		props.put("metadata.broker.list", "metadata.broker.list");

		final Consumer<Long, String> consumer = new KafkaConsumer<>(props);

		consumer.subscribe(Collections.singletonList(TOPIC));
		return consumer;
	}

	static List<ImpressionEvent> runConsumer(JavaSparkContext sc, TransportClient client) throws InterruptedException {
		
		final Consumer<Long, String> consumer = createConsumer();
		final int giveUp = 100;
		int noRecordsCount = 0;

		ImpressionEvent impEvent = new ImpressionEvent();
		List<ImpressionEvent> impEventList = new ArrayList<ImpressionEvent>();

		AdEvent adEvent = new AdEvent();

		while (true) {
			final ConsumerRecords<Long, String> consumerRecords = consumer.poll(1000);
			if (consumerRecords.count() == 0) {
				noRecordsCount++;
				if (noRecordsCount > giveUp)
					break;
				else
					continue;
			}
			consumerRecords.forEach(record -> {
				
				strTemp = record.value().substring(2, record.value().length() - 2);
				event = strTemp.split(",");
				int count = 1;

				if (event.length > 2) {

					impEventList.clear();
					for (String item : event) {
						keyValue = item.split(":")[1];
						if (keyValue.startsWith("\\"))
							keyValue = keyValue.substring(2, keyValue.length() - 2);

						if (count == 1)
							impEvent.setRequestid(keyValue);
						else if (count == 2)
							impEvent.setAdid(keyValue);
						else if (count == 3)
							impEvent.setAdtitle(keyValue);
						else if (count == 4)
							impEvent.setAdvertisercost(Double.parseDouble(keyValue));
						else if (count == 5)
							impEvent.setAppid(keyValue);
						else if (count == 6)
							impEvent.setApptitle(keyValue);
						else if (count == 7)
							impEvent.setImpressiontime(Long.parseLong(keyValue));

						count++;

					}

					impEventList.add(impEvent);
					JavaRDD<ImpressionEvent> impEventRDD = sc.parallelize(impEventList);

					javaFunctions(impEventRDD).writerBuilder("tapsell_event", "impression_event_table",
							CassandraJavaUtil.mapToRow(ImpressionEvent.class)).saveToCassandra();

				} else {

					keyValue = event[0].split(":")[1];
					if (keyValue.startsWith("\\"))
						reqId‬‬ = keyValue.substring(2, keyValue.length() - 2);

					clickTime‬‬ = Long.parseLong(event[1].split(":")[1]);

					JavaRDD<ImpressionEvent> ieRDD = javaFunctions(sc)
							.cassandraTable("tapsell_event", "impression_event_table",
									CassandraJavaUtil.mapRowTo(ImpressionEvent.class))
							.where("requestid=?", reqId‬‬).select("requestid", "adid", "adtitle", "advertisercost",
									"appid", "apptitle", "impressiontime");

				
					ieRDD.foreach(data -> {

						adEvent.setRequestId(data.getRequestid());
						adEvent.setAdId(data.getAdid());
						adEvent.setAdTitle(data.getAdtitle());
						adEvent.setAdvertiserCost(data.getAdvertisercost());
						adEvent.setAppId(data.getAdid());
						adEvent.setAppTitle(data.getAdtitle());
						adEvent.setImpressionTime(data.getImpressiontime());
						adEvent.setClickedTime(clickTime‬‬);

					});

					JSONObject dataAsJson = new JSONObject(adEvent);
					HashMap<String, Object> dataAsMap = new HashMap<String, Object>(dataAsJson.toMap());

					IndexResponse response = client.prepareIndex(indexName, "adEvent").setSource(dataAsMap, XContentType.JSON).execute()
							.actionGet();
				}

			});
			consumer.commitAsync();

		}
		consumer.close();
		return impEventList;
	}

	public static void main(String[] args) throws Exception {

		SparkConf conf = new SparkConf();
		conf.setAppName("Save Event In Cassandra");
		conf.setMaster("local");
		conf.set("spark.cassandra.connection.host", "127.0.0.1");
		conf.set("cassandra.connection.native.port", "9042");
		JavaSparkContext sc = new JavaSparkContext(conf);

		CassandraConnector connector = CassandraConnector.apply(sc.getConf());

		try {
			Session session = connector.openSession();

			session.execute("DROP KEYSPACE IF EXISTS tapsell_event");
			session.execute(
					"CREATE KEYSPACE tapsell_event WITH replication = {'class': 'SimpleStrategy', 'replication_factor': 1}");
			session.execute(
					"CREATE TABLE tapsell_event.impression_event_table (requestid TEXT PRIMARY KEY, adid TEXT, adtitle TEXT,"
							+ " advertisercost DOUBLE, appid TEXT, apptitle TEXT, impressiontime BIGINT)");
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}

		// on startup
		System.setProperty("es.set.netty.runtime.available.processors", "false");

		TransportClient client = null;
		try {		

			//The web URL you can open is localhost:9200, but the node's port is 9300
			client = new PreBuiltTransportClient(Settings.EMPTY)
					.addTransportAddress(new TransportAddress(InetAddress.getByName("localhost"), 9300));
						
		} catch (Exception e) {
			e.printStackTrace();
		}	

		List<ImpressionEvent> impEventList = runConsumer(sc, client);

		sc.stop();

		// on shutdown
		client.close();

	}

}
