package ir.tapsell.saveEventInKafka.controllers


import ir.tapsell.saveEventInKafka.entities.ClickEvent
import ir.tapsell.saveEventInKafka.services.ClickEventServices
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping
open class ClickEventController {



    @Autowired
    lateinit var clickEventServices: ClickEventServices

    @PostMapping("/clickEvent")

    fun greeting(@RequestBody clickEvent: ClickEvent): ClickEvent {
        clickEventServices.saveEvent(clickEvent)
		return clickEvent
	}
             

}