package ir.tapsell.saveEventInKafka.controllers


import ir.tapsell.saveEventInKafka.entities.ImpressionEvent
import ir.tapsell.saveEventInKafka.services.ImpressionEventServices
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping
open class ImpressionEventController {



    @Autowired
    lateinit var impressionEventServices: ImpressionEventServices

    @PostMapping("/impressionEvent")

    fun greeting(@RequestBody impressionEvent: ImpressionEvent): ImpressionEvent {
        impressionEventServices.saveEvent(impressionEvent)
		return impressionEvent
	}
             

}