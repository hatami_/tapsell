package ir.tapsell.saveEventInKafka

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.cloud.stream.annotation.EnableBinding
import org.springframework.kafka.annotation.EnableKafka
import org.springframework.cloud.stream.messaging.Source


//@SpringBootApplication
//@EnableKafka
//@EnableBinding( Source::class)
//open class SaveEventApplication

//      fun main(args: Array<String>) {

//                SpringApplication.run(SaveEventApplication::class.java, *args)
//            }


import org.springframework.boot.CommandLineRunner
//import org.springframework.boot.builder.SpringApplicationBuilder
import org.springframework.boot.web.support.SpringBootServletInitializer
import org.springframework.context.annotation.Bean
import ir.tapsell.saveEventInKafka.services.ImpressionEventServices
import ir.tapsell.saveEventInKafka.entities.ImpressionEvent
import ir.tapsell.saveEventInKafka.services.ClickEventServices
import ir.tapsell.saveEventInKafka.entities.ClickEvent

@SpringBootApplication
@EnableKafka
@EnableBinding(Source::class)
class SaveEventApplication : SpringBootServletInitializer() {
	@Bean
	fun init(obj1: ImpressionEventServices , obj2: ClickEventServices) = CommandLineRunner {

		var count = 1
	
		while (count < 20){
			
			obj1.saveEvent(ImpressionEvent(requestId = "reqId$count" , adId = "adId$count", adTitle = "adTitle$count",
					advertiserCost = 0.0, appId = "appId$count", appTitle = "appTitle$count", impressionTime = 1L))
			obj2.saveEvent(ClickEvent(requestId = "reqId$count", clickTime = 1L))
				
			count +=1
		}

	}

}


fun main(args: Array<String>) {
	SpringApplication.run(SaveEventApplication::class.java, *args)
}
