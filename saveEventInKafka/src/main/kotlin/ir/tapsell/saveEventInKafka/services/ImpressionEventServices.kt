package ir.tapsell.saveEventInKafka.services


import ir.tapsell.saveEventInKafka.entities.ImpressionEvent

interface ImpressionEventServices {

    fun saveEvent(event: ImpressionEvent)
}

