package ir.tapsell.saveEventInKafka.services


import ir.tapsell.saveEventInKafka.entities.ClickEvent

interface ClickEventServices {

    fun saveEvent(event: ClickEvent)
}

