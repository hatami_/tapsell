package ir.tapsell.saveEventInKafka.entities


data class ClickEvent(
		
	val requestId: String, // a unique id for the request
	val clickTime: Long)


